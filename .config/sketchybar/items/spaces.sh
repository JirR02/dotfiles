#!/bin/sh

sketchybar --add event aerospace_workspace_change

SPACE_STRS=(    󰏆   󰐫 )

for str in "${SPACE_STRS[@]}"
do
  sketchybar -m --add space $str left                                 \
                --subscribe $str aerospace_workspace_change \
                --set $str icon=$str                                  \
                        associated_display=1 \
                        label.font="sketchybar-app-font:Regular:16.0" \
                        label.padding_right=20 \
                        label.y_offset=-1\
                        script="$PLUGIN_DIR/space.sh $str"
done
