#!/bin/bash

source "$CONFIG_DIR/colors.sh"

if [ "$1" = "$FOCUSED_WORKSPACE" ]; then
    sketchybar --set $NAME background.drawing=on \
                     background.color=$ACCENT_COLOR \
                     label.color=$BAR_COLOR \
                     icon.color=$BAR_COLOR
else
    sketchybar --set $NAME background.drawing=off \
                     label.color=$ACCENT_COLOR \
                     icon.color=$ACCENT_COLOR
fi

if [ "$SENDER" = "aerospace_workspace_change" ]; then

  apps=$(aerospace list-windows --workspace $1 | awk -F'|' '{gsub(/^ *| *$/, "", $2); print $2}')

  icon_strip=" "
  if [ "${apps}" != "" ]; then
    while read -r app
    do
      icon_strip+=" $($CONFIG_DIR/plugins/icon_map_fn.sh "$app")"
    done <<< "${apps}"
  else
    icon_strip=" —"
  fi

  sketchybar --set $1 label="$icon_strip"

fi
