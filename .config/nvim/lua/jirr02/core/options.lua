local opt = vim.opt

opt.relativenumber = true
opt.number = true

-- tabs & indentation
opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

-- disable line wrapping
opt.wrap = false

-- search settings
opt.ignorecase = true
opt.smartcase = true

-- undo
opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
opt.undofile = true

-- search behaviour
opt.hlsearch = false
opt.incsearch = true

-- cursor always stays in the middle
opt.scrolloff = 999

-- colorscheme
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes"

-- backspace
opt.backspace = "indent,eol,start"

-- clipboard
opt.clipboard:append("unnamedplus")

-- splitting windows
opt.splitright = true
opt.splitbelow = true

opt.iskeyword:append("-")
