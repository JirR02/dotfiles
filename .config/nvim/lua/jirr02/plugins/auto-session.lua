return {
	"rmagatti/auto-session",
	config = function()
		local auto_session = require("auto-session")
		local wk = require("which-key")

		auto_session.setup({
			auto_restore_enabled = false,
		})

		wk.add({
			{ "<leader>s", group = "session", icon = "" },
			{ "<leader>sr", "<cmd>SessionRestore<CR>", desc = "Restore session for cwd", icon = "󰦛" },
			{ "<leader>sr", "<cmd>SessionSave<CR>", desc = "Save Session", icon = "" },
		})
	end,
}
