return {
	"nvim-telescope/telescope.nvim",
	branch = "0.1.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		{ "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
		"nvim-tree/nvim-web-devicons",
		"nvim-telescope/telescope-live-grep-args.nvim",
	},
	config = function()
		local telescope = require("telescope")
		local wk = require("which-key")

		telescope.load_extension("fzf")
		telescope.load_extension("find_template")

		wk.add({
			{ "<leader>f", group = "telescope", icon = "" },
			{
				"<leader>ff",
				"<cmd>Telescope find_files<CR>",
				desc = "Find Files in Current Working Directory",
				icon = "",
			},
			{ "<leader>fr", "<cmd>Telescope oldfiles<CR>", desc = "Recent Files", icon = "󰙰" },
			{ "<leader>fs", "<cmd>Telescope live_grep<CR>", desc = "Find String in CWD", icon = "" },
			{ "<leader>fc", "<cmd>Telescope grep_string<CR>", desc = "Find String in File", icon = "" },
			{ "<leader>fg", "<cmd>Telescope git_files<CR>", desc = "Find git files", icon = "" },
			{ "<leader>ft", "<cmd>TodoTelescope<CR>", desc = "Find todos", icon = "" },
		})
	end,
}
