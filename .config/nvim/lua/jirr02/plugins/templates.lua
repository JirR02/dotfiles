return {
	"nvimdev/template.nvim",
	cmd = { "Template", "TemProject" },
	config = function()
		local template = require("template")
		local wk = require("which-key")

		template.setup({
			temp_dir = "~/.config/nvim/templates/",
		})

		wk.add({
			{ "<leader>t", group = "template", icon = "" },
			{
				"<leader>ti",
				"<cmd>Telescope find_template type=insert filter_ft=false<CR>",
				desc = "Insert template",
				icon = "",
			},
			{ "<leader>tl", "<cmd>!theory<CR>", desc = "Insert latex notebook template", icon = "" },
		})
	end,
}
