return {
	"akinsho/bufferline.nvim",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
		"ojroques/nvim-bufdel",
	},
	config = function()
		local bufferline = require("bufferline")
		local wk = require("which-key")

		bufferline.setup({
			options = {
				mode = "buffers",
				seperator_style = "slant",
			},
		})

		wk.add({
			{ "<leader>b", group = "buffer", icon = "󰓩" },
			{ "<leader>bx", "<cmd>BufDel<CR>", desc = "Close Buffer", icon = "󰭌" },
			{ "<leader>bn", "<cmd>BufferLineCycleNext<CR>", desc = "Go to next buffer", icon = "󰌒" },
			{ "<leader>bp", "<cmd>BufferLineCyclePrev<CR>", desc = "Go to previous buffer", icon = "󰌥" },
		})
	end,
}
