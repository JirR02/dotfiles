return {
	"AckslD/nvim-FeMaco.lua",
  lazy = true,
  dependencies = { "nvim-tree/nvim-web-devicons" },
	config = function()
		local femaco = require("femaco")
		local wk = require("which-key")

		femaco.setup({})

		wk.add({
      {"<leader>me", "<cmd>FeMaco<CR>", desc = "Edit code block in buffer", icon = ""},
		})
	end,
}
