return {
	"neovim/nvim-lspconfig",
	event = { "BufReadPre", "BufNewFile" },
	dependencies = {
		"hrsh7th/cmp-nvim-lsp",
		"onsails/lspkind.nvim",
		{ "folke/neodev.nvim", opts = {} },
	},
	config = function()
		local lspconfig = require("lspconfig")
		local mason_lspconfig = require("mason-lspconfig")
		local cmp_nvim_lsp = require("cmp_nvim_lsp")
		local wk = require("which-key")

		local capabilities = cmp_nvim_lsp.default_capabilities()

		mason_lspconfig.setup_handlers({
			function(server_name)
				lspconfig[server_name].setup({
					capabilities = capabilities,
				})
			end,
		})

		lspconfig["lua_ls"].setup({
			capabilities = capabilities,
			settings = { -- custom settings for lua
				Lua = {
					-- make the language server recognize "vim" global
					diagnostics = {
						globals = { "vim" },
					},
					workspace = {
						-- make language server aware of runtime files
						library = {
							[vim.fn.expand("$VIMRUNTIME/lua")] = true,
							[vim.fn.stdpath("config") .. "/lua"] = true,
						},
					},
				},
			},
		})
		lspconfig["pyright"].setup({
			capabilities = capabilities,
			filetypes = { "python" },
		})

		wk.add({
			{ "<leader>l", group = "lsp", icon = "" },
			{ "<leader>lr", "<cmd>Telescope lsp_references<CR>", desc = "Show references", icon = "" },
			{ "<leader>lD", "<cmd>lua vim.lsp.buf.defenition()<CR>", desc = "Show defenition", icon = "" },
			{ "<leader>lc", "<cmd>lua vim.lsp.buf.code_action()<CR>", desc = "Code actions", icon = "" },
			{ "<leader>lR", "<cmd>lua vim.lsp.buf.rename()<CR>", desc = "Smart rename", icon = "󰑕" },
			{ "<leader>ld", "<cmd>Telescope diagnostics bufnr=0<CR>", desc = "Diagnostics", icon = "󰍉" },
			{ "<leader>ll", "<cmd>lua vim.lsp.buf.open_float()<CR>", desc = "Line Diagnostics", icon = "󰍉" },
			{ "<leader>ln", "<cmd>lua vim.diagnostics.goto_next()<CR>", desc = "Go to next Diagnostic", icon = "" },
			{
				"<leader>lp",
				"<cmd>lua vim.diagnostics.goto_prev()<CR>",
				desc = "Go to previous Diagnostic",
				icon = "",
			},
			{ "<leader>li", "<cmd>lua vim.lsp.buf.hover()<CR>", desc = "Show documentation", icon = "󰈙" },
			{ "<leader>lL", "<cmd>LspRestart<CR>", desc = "Restart LSP", icon = "" },
		})
	end,
}
