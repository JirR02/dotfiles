return {
	{ "nvim-lua/plenary.nvim" },
	{ "tpope/vim-surround" },
	{ "christoomey/vim-tmux-navigator" },
	{ "tpope/vim-fugitive" },
	{ "jghauser/follow-md-links.nvim" },
}
