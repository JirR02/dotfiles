return {
	"MeanderingProgrammer/markdown.nvim",
  event = { "BufReadPre", "BufNewFile" },
	dependencies = "nvim-treesitter/nvim-treesitter",
	config = function()
		local markdown = require("render-markdown")

		markdown.setup({
			checkbox = {
				custom = {
          inprogress = { raw = '[>]', rendered = '󰦕 ', highlight = 'RenderedMarkdownInProgress'},
          onhold  = { raw = '[=]', rendered = ' ', highlight = 'RenderedMarkdownOnhold'},
          cancelled = { raw = '[_]', rendered  = ' ', highlight = 'RenderedMarkdownCancelled'},
          important = { raw = '[!]', rendered = '', highlight = 'RenderedMarkdownImportant'},
          recurring = { raw = '[+]', rendered = ' ', highlight = 'RenderedMarkdownRecurring'},
          uncertain = { raw = '[?]', rendered = ' ', highlight = 'RenderedMarkdownUncertain'},
        },
			},
		})
	end,
}
