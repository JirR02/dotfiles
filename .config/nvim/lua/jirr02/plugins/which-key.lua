return {
	"folke/which-key.nvim",
	event = "VeryLazy",
	init = function()
		vim.o.timeout = true
		vim.o.timeoutlen = 500
	end,
	config = function()
		local wk = require("which-key")

		wk.add({
			{ "<leader>n", group = "vim", icon = "" },
			{ "<leader>nh", "<cmd>nohl<CR>", desc = "Close Search Highlights", icon = "" },
			{ "<leader>no", "<cmd>foldopen<CR>", desc = "Open Fold", icon = "" },
			{ "<leader>nc", "<cmd>foldclose<CR>", desc = "Close Fold", icon = "" },
			{ "<leader>s", group = "splits", icon = "󱓡" },
			{ "<leader>sv", "<C-w>v", desc = "Split vertical", icon = "" },
			{ "<leader>sh", "<C-w>s", desc = "Split horizontal", icon = "" },
			{ "<leader>se", "<C-w>=", desc = "Make Splits equal", icon = "󰤼" },
			{ "<leader>sx", "<cmd>close<CR>", desc = "Close Split", icon = "󰅚" },
			{ "<leader>i", group = "interface", icon = "󰮫" },
			{ "<leader>il", "<cmd>Lazy<CR>", desc = "Open Lazy Menu" },
			{ "<leader>im", "<cmd>Mason<CR>", desc = "Open Mason Menu" },
		})
	end,
}
