return {
	"lervag/vimtex",
	lazy = false,
	init = function()
		local wk = require("which-key")

		vim.g.vimtex_view_method = "zathura"
		vim.g.vimtex_quickfix_mode = 0
		vim.g.vimtex_compiler_latexmk = {
			options = {
				"-pdf",
			},
		}
    vim.g.vimtex_fold_enabled = true
    vim.g.vimtex_format_enabled = true

		wk.add({
			{ "<leader>v", group = "vimtex", icon = "" },
			{ "<leader>vc", "<cmd>VimtexCompile>CR>", desc = "Compile LaTex File", icon = "" },
			{ "<leader>vo", "<cmd>VimtexView<CR>", desc = "Open compiled PDF in Zathura", icon = "" },
			{ "<leader>vd", group = "delete", icon = "" },
			{ "<leader>vde", "<Plug>(vimtex-env-delete)", desc = "Delete surrounding environment" },
			{ "<leader>vc", group = "change", icon = "" },
			{ "<leader>vt", group = "toggle", icon = "" },
		})
	end,
}
