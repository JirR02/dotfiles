return {
	"Myzel394/easytables.nvim",
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		local easytables = require("easytables")
		local wk = require("which-key")

		easytables.setup({})

		wk.add({
			{ "<leader>m", group = "markdown", icon = "" },
			{
				"<leader>mt",
				function()
					vim.ui.input({ prompt = "width x height: " }, function(input)
						vim.cmd("EasyTablesCreateNew " .. input)
					end)
				end,
			},
		})
	end,
}
