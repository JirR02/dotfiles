return {
	"stevearc/conform.nvim",
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		local conform = require("conform")
    local wk = require("which-key")

		conform.setup({
			formatters_by_ft = {
				markdown = { "prettier" },
				python = { "black" },
				lua = { "stylua" },
			},
		})

    wk.add({
      { "<leader>l" , group = "lsp", icon = ""},
      {"<leader>lf", function() conform.format({lsp_fallback = true, async = false, timeout_ms = 1000}) end, desc = "Format file", icon = "󰛖"},
    })
	end,
}
