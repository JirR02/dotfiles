{
description = "JirR02 Darwin system flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
    nix-homebrew.url = "github:zhaofengli-wip/nix-homebrew";
  };

  outputs = inputs@{ self, nix-darwin, nixpkgs, nix-homebrew }:
  let
    configuration = { pkgs, config, ... }: {
    
      nixpkgs.config.allowUnfree = true;
      # List packages installed in system profile. To search by name, run:
      # $ nix-env -qaP | grep wget
      environment.systemPackages =
        [ pkgs.bat
          pkgs.cowsay
          pkgs.neovim
          pkgs.docker
          pkgs.eza
          pkgs.feh
          pkgs.fzf
          pkgs.gcc
          pkgs.git
          pkgs.mkalias
          pkgs.nodejs_22
          pkgs.oh-my-posh
          pkgs.openssh
          pkgs.ripgrep
          pkgs.ruff-lsp
          pkgs.stow
          pkgs.sqlite
          pkgs.sqlitebrowser
          pkgs.unixODBCDrivers.sqlite
          pkgs.tailscale
          pkgs.thefuck
          pkgs.tmux
          pkgs.tree
          pkgs.unzip
          pkgs.vifm
          pkgs.wget
          pkgs.zip
          pkgs.zoxide
          pkgs.sketchybar
          pkgs.aerospace
          pkgs.alacritty
          pkgs.arc-browser
#          pkgs.blender
          pkgs.discord
          pkgs.docker-client
          pkgs.inkscape
#          pkgs.kicad
          pkgs.signal-desktop
          pkgs.raycast
          pkgs.spotify
          pkgs.zathura
        ];

         homebrew = {
           enable = true;
           brews = [
             "mas"
             "python"
           ];
           casks = [
            "alt-tab"
            "bricklink-studio"
            "font-meslo-lg-nerd-font"
            "ultimaker-cura"
            "ltspice"
	          "jabref"
	          "kicad"
	          "microsoft-teams"
            "mactex-no-gui"
	          "nextcloud"
	          "ultimaker-cura"
	          "whatsapp"
           ];
           masApps = {
            "GoodNotes 6" = 1444383602;
            "Word" = 462054704;
            "PowerPoint" = 462062816;
            "Excel" = 462058435;
            "Outlook" = 985367838;
            "OneDrive" = 823766827;
           };
           onActivation.cleanup = "zap";
         };

        system.activationScripts.applications.text = let
          env = pkgs.buildEnv {
            name = "system-applications";
            paths = config.environment.systemPackages;
            pathsToLink = "/Applications";
          };
        in
          pkgs.lib.mkForce ''
          # Set up applications.
          echo "setting up /Applications..." >&2
          rm -rf /Applications/Nix\ Apps
          mkdir -p /Applications/Nix\ Apps
          find ${env}/Applications -maxdepth 1 -type l -exec readlink '{}' + |
          while read src; do
            app_name=$(basename "$src")
            echo "copying $src" >&2
            ${pkgs.mkalias}/bin/mkalias "$src" "/Applications/Nix Apps/$app_name"
          done
        '';

      # Auto upgrade nix package and the daemon service.
      services.nix-daemon.enable = true;
      # nix.package = pkgs.nix;

      services.sketchybar.enable = true;
      services.tailscale.enable = true;

      # Necessary for using flakes on this system.
      nix.settings.experimental-features = "nix-command flakes";

      # Create /etc/zshrc that loads the nix-darwin environment.
      programs.zsh.enable = true;  # default shell on catalina
      # programs.fish.enable = true;

      # Set Git commit hash for darwin-version.
      system.configurationRevision = self.rev or self.dirtyRev or null;

      # Used for backwards compatibility, please read the changelog before changing.
      # $ darwin-rebuild changelog
      system.stateVersion = 5;

      # The platform the configuration will be used on.
      nixpkgs.hostPlatform = "aarch64-darwin";
    };
  in
  {
    # Build darwin flake using:
    # $ darwin-rebuild build --flake .#simple
    darwinConfigurations."mbpro" = nix-darwin.lib.darwinSystem {
      modules = [ 
        configuration 
        nix-homebrew.darwinModules.nix-homebrew
        {
        nix-homebrew = {
          # Install Homebrew under the default prefix
          enable = true;

          # Apple Silicon Only: Also install Homebrew under the default Intel prefix for Rosetta 2
          enableRosetta = true;

          # User owning the Homebrew prefix
          user = "jirayu";

          # Automatically migrate existing Homebrew installations
          autoMigrate = true;
        };
      }
   ]; 

  };

    # Expose the package set, including overlays, for convenience.
    darwinPackages = self.darwinConfigurations."mbpro".pkgs;
  };
}
